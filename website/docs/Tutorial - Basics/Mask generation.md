---
sidebar_position: 4
---

# Feature C : Mask generation

The mask generation tab is used for designing mask patterns and generating associated filtering cube.

![Mask design tab](/img/mask_design.svg "Mask design tab")

## 1 - System Settings 

Located on the left side of the application window.

**Be Careful in your dimensioning : it defines the dimensions of the filtering cube **

Includes :

- **detector** : parameters that define the detector grid
    - *sampling along X* : number of pixels along the X dimension
    - *sampling along Y* : number of pixels along the Y dimension
    - *delta x* : size of a pixel along the X dimension
    - *delta y* : size of a pixel along the Y dimension
- **SLM** : parameters that define the mask grid 
    - *sampling along X* : number of mask elements along the X dimension
    - *sampling along Y* : number of mask elements along the Y dimension
    - *delta x* : size of a mask element along the X dimension
    - *delta y* : size of a mask element along the Y dimension
- **spectral range**: the spectral boundaries of the system and the number of spectral bands to consider
    - *wavelength min* : lowest wavelength to enter the optical system
    - *wavelength max* : highest wavelength to enter the optical system
    - *number of spectral samples* : number of spectral bands to consider.


## 2 - Mask Settings

Located on the left side of the application window.


The masks caracteristics depend on the chosen mask type.

Availaible mask patterns :

- **slit** : only one column of the mask is open (perpendicular to the spectral dispersion), thus generating a spectral gradient type filter.
    - *slit position* : relative to the center column between -100 and 100 mask element
    - *slit width* : between 1 and 30mask elements.

- **random** : random noise pattern with a normal law

- **blue noise** : random noise pattern with boosted high frequencies 

## 3 - Generate Filtering Cube button

By clicking on this button a 2D array representing a mask pattern is generated through mask generation functions contained in the `CassiSystem` class. 

By clicking on this button, a `CassiSystem` instance is creating the filtering cube corresponding to the **detector dimensions along  X and Y** and the **number of spectral bands**.

Each slice of the filtering contains the projection of the mask pattern

## 4 - Display Mask and Filtering Cube

Located on the right side of the application window.

### Mask Grid 

Shows the generated (or loaded) mask grid :

![Docusaurus logo](/img/mask_display.svg)

### Filtering Cube, slice by slice

Shows the corresponding filtering cube, slice by slice : 

![Docusaurus logo](/img/filtering-cube.svg)

