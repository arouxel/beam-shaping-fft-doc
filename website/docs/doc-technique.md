---
sidebar_position: 1
---

# Physics & Technical Information 


![Beam shaper](/img/simple-shaper.svg)

Details regarding the theoretical background of the simulations can be found in this [pdf file](https://cloud.laas.fr/index.php/s/r2NLpUHs0I2BG2u).