import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Generate Input Field',
    Svg: require('@site/static/img/input_beam.svg').default,
    description: (
      <>
        Laser input fields can be generated from a variety of sources: Gaussian, Hermite-Gaussian, Laguerre-Gaussian.
      </>
    ),
  },
  {
    title: 'Design Target Field',
    Svg: require('@site/static/img/target_amplitude.svg').default,
    description: (
      <>
        The target field can be designed using operations ("+","-","x",....) on arrays. 
      </>
    ),
  },
  {
    title: 'Design Phase Mask',
    Svg: require('@site/static/img/mask_design2.svg').default,
    description: (
      <>
        Phase masks are designed using operations ("+","-","x",....) on arrays.
      </>
    ),
  },
  {
    title: 'Propagation',
    Svg: require('@site/static/img/fourier_plane.svg').default,
    description: (
      <>
       The modulated field is propagated through the optical system and can be analyzed at various planes.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
